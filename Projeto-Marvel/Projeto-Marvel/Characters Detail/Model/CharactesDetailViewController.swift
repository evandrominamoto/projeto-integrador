//
//  CharactesDetailViewController.swift
//  Projeto-Marvel
//
//  Created by Jhonathan Mattos on 16/11/21.
//

import UIKit

class CharactesDetailViewController:UIViewController {
    
    @IBOutlet weak var charactersImageView: UIImageView!
    @IBOutlet weak var detailsLabel: UILabel!
    
    var character: Character?
    
    
    func setup(value: Character){
//        self.charactersImageView.image = UIImage(named: self.character?.[thumbnail:thumbnail] ?? "")
//        self.detailsLabel.text = self.character?.description
    }
    
    func extractImage(data: [String: String]) ->URL? {
        let path = data["path"] ?? ""
        let ext = data["extension"] ?? ""
        return URL(string: "\(path).\(ext)")
    }
}
