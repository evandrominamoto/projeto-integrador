//
//  SearchHomeVC.swift
//  Projeto-Marvel
//
//  Created by Jhonathan Mattos on 28/08/21.
//  Recreation by Evandro Minamoto on 30/10/21.

import Foundation
import UIKit


class SeriesViewController: UIViewController {
    var seriesController = SeriesController()
    
    @IBOutlet weak var seriesTableView: UITableView!

    @IBOutlet weak var seriesSearchBar: UISearchBar!
    
    override func viewDidLoad() {
        self.seriesTableView.delegate = self
        self.seriesTableView.dataSource = self
        self.seriesSearchBar.delegate = self
        self.seriesTableView.register(UINib(nibName: "SeriesTableViewCell", bundle: nil), forCellReuseIdentifier: "SeriesTableViewCell")
        
        self.seriesController.getSeries(name: nil){ result, error in
            if result{
                print("============SERIES ========Data OK=============")
                self.seriesTableView.reloadData()
            }else{
                print(error)
            }
        }
    }
}

//MARK: - Cell Methods

extension SeriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SeriesTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "SeriesTableViewCell", for: indexPath) as? SeriesTableViewCell
        return cell ?? UITableViewCell()
        
    }
    
    
    
}
extension SeriesViewController: UISearchBarDelegate{
    
}
