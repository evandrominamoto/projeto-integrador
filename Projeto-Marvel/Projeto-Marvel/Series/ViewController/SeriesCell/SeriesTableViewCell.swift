//
//  SeriesTableViewCell.swift
//  Projeto-Marvel
//
//  Created by Evandro Rodrigo Minamoto on 30/10/21.
//

import UIKit

class SeriesTableViewCell: UITableViewCell {

    @IBOutlet weak var seriesView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.seriesView.borderView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
