//
//  HomeVC.swift
//  Projeto-Marvel
//
//  Created by Jhonathan Mattos on 28/08/21.
//  Recreation by Evandro Minamoto on 30/10/21.

import Foundation
import UIKit


class CharacterVC: UIViewController {
    
    let characterController = CharacterController()
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var characterSearchBar: UISearchBar!
    
    override func viewDidLoad() {
        self.characterSearchBar.borderView(bordeColor: UIColor.black, borderWidth: 1)
        self.characterSearchBar.layer.cornerRadius = 10
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        self.characterSearchBar.delegate = self
        self.homeTableView.register(UINib(nibName: "CharacterTableViewCell", bundle: nil), forCellReuseIdentifier: "CharacterTableViewCell")
        self.characterController.networkCharacters(name: nil) { result, error in
            if result{
                print("Data OK")
                self.homeTableView.reloadData()
            }else{
                print(error)
            }
        }
    }

}

// MARK: - Cell Methods 
extension CharacterVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characterController.getCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CharacterTableViewCell? = homeTableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell", for: indexPath) as? CharacterTableViewCell
        cell?.setup(value: characterController.getCharacter(indexPath: indexPath))
        return cell ?? UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
//        let characterSelected: Character  = self.[indexPath.row]
        
//        performSegue(withIdentifier: "DetailViewController", sender: characterSelected)
    }
    
}


extension CharacterVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        characterController.networkCharacters(name: self.characterSearchBar.text) { result, error in
            if result{
                self.homeTableView.reloadData()
            }else{
                print(error)
            }
        }
    }
}
